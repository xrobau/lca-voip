<?php
require 'vendor/autoload.php';
require 'config/config.php';
error_reporting(-1);
?>
<!DOCTYPE html>
<html>
  <head>
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="/css/materialize.min.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="/css/lca.css"  media="screen,projection"/>

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>
<?php
include "views/header.php";
switch ($_SERVER['REQUEST_URI']) {
case "/":
	include "views/body.php";
	break;
case "/manage":
case "/directory":
case "/requestuuid":
case "/contact":
case "/legal":
case "/aosp":
case "/pokemon":
	include "views".$_SERVER['REQUEST_URI'].".php";
	break;
default:
	if (strpos($_SERVER['REQUEST_URI'], "/manage/") === 0) {
		include "views/manageauth.php";
		break;
	}
	echo "404?\n";
}
include "views/footer.php";
include "views/scripts.php";
?>
  </body>
</html>
        

