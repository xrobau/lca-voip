<?php

// Copy this to config.php and change it to suit your setup

// This is the local machine, it syncs from gDocs
define('DBHOST', 'localhost');
define('DBNAME', 'lca');
define('DBUSER', 'lcauser');
define('DBPASS', 'PASSWORDPASSWORDPASSW');

// The gDocs Spreadsheet ID
define('SPREADSHEET', '12345671234567123456712345671234567123456712');

// Main delegate Asterisk server
define('ASTERISKIP', '180.149.231.107');
define('AMIUSER', 'lca');
define('AMIPASS', 'PASSWORDPASS');

// The MySQL credentials for that machine
define('FPBXDBUSER', 'freepbxuser');
define('FPBXDBPASS', '12345671234567123456712345671234');

// UUID used for auth to the FreePBX machine, put this into
// /etc/api.key on that machine, and install tools/voicemailapi.php
// into /var/www/html
define('FPBXAPIKEY', '8a4e05a3-5ad9-491b-8790-70661f5cece9');



