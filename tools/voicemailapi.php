<?php

if (!file_exists("/etc/api.key")) {
	print "No API key. Generate it, and stick it in place.\n";
	exit;
}

include "/etc/api.key";

if (empty($_POST['apikey']) || $_POST['apikey'] !== $apikey) {
	print "API Key auth error.\n";
	exit;
}

$bootstrap_settings['freepbx_auth'] = false;

include '/etc/freepbx.conf';

if (empty($_POST['exten'])) {
	print "Wtf\n";
	exit;
}

$exten = $_POST['exten'];
$action = $_POST['action'];
$displayname = 'Unconfigured User';
$email = "";
$vmpin = "8675309";

if (!empty($_POST['displayname'])) {
	$displayname = $_POST['displayname'];
}

if (!empty($_POST['email'])) {
	$email = $_POST['email'];
}

if (!empty($_POST['vmpin'])) {
	$vmpin = $_POST['vmpin'];
}
$vmx = \FreePBX::Voicemail();

$vmx->delMailbox($exten);

$p = \FreePBX::Database()->prepare('UPDATE `users` SET `voicemail`=? WHERE `extension`=?');

if ($action == "disable") {
	$p->execute(['novm', $exten]);
} else {
	$settings = [
		"vmcontext" => "default",
		"vm" => "enabled",
		"vmpwd" => $vmpin,
		"name" => $displayname,
		"email" => $email,
		"attach" => "attach=yes",
		"saycid" => "saycid=yes",
		"envelope" => "envelope=no",
		"delete" => "delete=no",

	];

	$p->execute(['default', $exten]);
	$vmx->addMailbox($exten, $settings);
}

`asterisk -rx 'voicemail reload'`;




