<?php

$url = "https://voip.linux.conf.au/manage/$invitetoken";

$text = "Welcome to Linux.conf.au 2019!

This email provides you with a link to manage your VoIP account. Do not give this link to anyone else!

Use this link to manage your account: $url

If you have no idea what this email is about, you can happily ignore it.

Thanks, and we hope you enjoy LCA 2019!

";

$html = "<h1>Welcome to Linux.conf.au 2019!</h1>
	<p>This email provides you with a link to manage your VoIP account. Do not give this link to anyone else!</p>
	<p>Use this link to manage your account: <a href='$url'>$url</a></p>
	<p>If you have no idea what this email is about, you can happily ignore it.</p>
	<p>Thanks, and we hope you enjoy LCA 2019!</p>

	";


