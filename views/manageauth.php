<?php

$tmp = explode('/', $_SERVER['REQUEST_URI']);
if (empty($tmp[2]) || strlen($tmp[2]) !== 36) {
	print "Something strange happened. Can't continue\n";
	return;
}

$uuid = $tmp[2];

$lca = new xrobau\LCA;
$details = $lca->get_from_uuid($uuid);

if (!$details) {
	print "<div class='container'><div class='card'><div class='card-content'>UUID Error. Unknown error. Can not continue. Check your link and try again.</div></div></div>";
	return;
}

$exten = $details['exten'];
$displayname = $details['displayname'];
$vmemail = $details['vmemail'];
$directory = $details['directory'];
$vmpin = $details['vmpin'];
$gravatar = $details['gravatar'];
$misctext = $details['misctext'];

// Has a POST been submitted with an update?
if ($_SERVER['REQUEST_METHOD'] === "POST") {
	$newdisplayname = $_POST['displayname']??false;
	$newvmemail = $_POST['vmemail']??false;
	$newdirectory = $_POST['publish']??'0';
	$newvmpin = $_POST['vmpin']??null;
	$newgravatar = $_POST['gravatar']??null;
	$newmisc = $_POST['misctext']??'';

	if ($newdisplayname !== $displayname) {
		$displayname = $lca->update_displayname($uuid, $newdisplayname);
	}

	if ($newvmemail !== $vmemail || $newvmpin != $vmpin) {
		$vmemail = $lca->update_voicemail($uuid, $newvmemail, $displayname, $newvmpin);
		$vmpin = $newvmpin;
	}

	if ($newdirectory !== $directory) {
		$directory = $lca->update_directory($uuid, $newdirectory);
	}

	if ($newgravatar !== $gravatar) {
		$gravatar = $lca->update_gravatar($uuid, $newgravatar);
	}

	if ($newmisc !== $misctext) {
		$misctext = $lca->update_misctext($uuid, $newmisc);
	}

}

if (!$displayname) {
	$displayname = '';
}

if (!$vmemail) {
	$vmemail = '';
}

if (!$vmpin) {
	$vmpin = '';
}

if ($directory == 1) {
	$dcheckbox = "checked";
} else {
	$dcheckbox = "";
}

?>

<div class='row'>
  <div class='col s12'>
    <h4>Extension <?= $exten ?> Configuration</h4>
    <div class='card'>
      <div class='card-content'>
        <span class='card-title'>Configuration</span>
        <p>Your Public name will be presented to people when you call them internally. It is not used for external calls. It is also what is searchable if you opt-in to the Directory. It is limited to 7-bit ASCII via the web interface. UTF8 is available on an as-needed basis, if required.</p>
        <p>If you provide an email address, any voicemails received will be sent to that address. Removing the PIN disables Voicemail on your extension</p>
      </div>
    </div>
  </div>
</div>
<div class='row'>
  <form class='col s12' method='post'>
    <div class='row'>
      <div class='input-field col s12'>
	<input id='displayname' name='displayname' type="text" value='<?= $displayname ?>'>
        <label for='displayname'>Public 'Caller ID Name'</label>
      </div>
      <div class='input-field col s12 m8'>
	<input id='vmemail' name='vmemail' type="email" value='<?= $vmemail ?>'>
        <label for='pass'>Enter an Email address for Voicemail to be sent to</label>
      </div>
      <div class='input-field col s12 m4'>
	<input placeholder='Voicemail disabled without PIN' id='vmpin' name='vmpin' type="text" pattern='[0-9]{6}' value='<?= $vmpin ?>' >
        <label for='displayname'>Your Voicemail PIN (6 digits)</label>
      </div>
      <div class='col s6'>Publish in Directory</div>
      <div class='col s6'>
        <div class='right'>
          <div class="switch">
	    <label> Off <input type="checkbox" name='publish' <?= $dcheckbox ?>> <span class="lever"></span> On </label>
          </div>
        </div>
      </div>
      <div class='input-field col s12'>
	<input id='gravatar' name='gravatar' type="email" value='<?= $gravatar ?>'>
        <label for='gravatar'>Enter an Gravatar Email address for the Directory</label>
      </div>
      <div class='input-field col s12'>
	<textarea id='misctext' name='misctext' class="materialize-textarea"><?= $misctext ?></textarea>
        <label for='misctext'>Enter any other information you wish to appear in the directory. Markdown format is supported.</label>
      </div>
    </div>
    </div>
    <div class='row'>
      <div class='col s12'>
        <button class=" btn waves-effect waves-light" type="submit" name="action" value="submit">Update <i class="material-icons right">send</i> </button>
      </div>
    </div>
  </form>
</div>

