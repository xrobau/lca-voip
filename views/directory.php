<?php
$l = new xrobau\LCA;
print "<script> window.directory = ".json_encode($l->get_all_dir_entries())." </script>\n";
?>

<div class="row">
  <div class="col s12 m8">
    <h4>Directory Search</h4>
    <div class="row">
      <div class="input-field col s12">
        <i class="material-icons prefix">face</i>
        <input type="text" id="dirsearch" class="autocomplete">
        <label for="dirsearch">Start typing a name...</label>
      </div>
    </div>
  </div>
  <div class='col m4 hide-on-small-only'>
    <a href='https://faktortel.com.au/business-voip/?utm_source=linuxconf&utm_medium=mgmtpc&utm_campaign=freesetup#plans' target='_new'><img class='responsive-img' src='/img/LinuxConfAU_FaktorTel_Promo.jpg'></a>
  </div>
  <div class="col s12 hide isdir">
    <div class='card'>
      <div class='card-content'>
        <span class='card-title'>Extension <span id="direxten">&nbsp;</span> - <span id="displayname"> &nbsp; </span></span>
        <p id="markdown"> &nbsp; </p>
      </div>
    </div>
  </div>
  <div class='col s12 show-on-small hide-on-med-and-up'>
    <a href='https://faktortel.com.au/business-voip/?utm_source=linuxconf&utm_medium=mgmtmobile&utm_campaign=freesetup#plans' target='_new'><img class='responsive-img' src='/img/LinuxConfAU_FaktorTel_Promo.jpg'></a>
  </div>
</div>


