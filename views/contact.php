<div class='container'>
  <div class='row'>
    <div class='col s12'>
      <div class='card'>
        <div class='card-content'>
          <span class='card-title'>Contact Information</span>
          <div class='card'><div class='card-content'>
	  <p><b>If you are not on campus and there is an emergency, Dial 111 (NZ Emergency) or 112 (From any Mobile phone)</b>.</p>
	  <p> If you are on Campus, call <tt>92111</tt> from any Campus phone. If you are unable to reach a Campus phone, you can contact them 
	      on <tt>0800 823 637</tt>.</p>
	  <p>UC Campus has their own medical team, and can respond to life-threatening situations much faster than 111, and will also liase with
             the Emergency Services to ensure they are directed to the correct place on campus.</p>
          </div></div>
	  <p>This is provided as an unofficial service by <a href='https://twitter.com/xrobau'>Rob Thomas (xrobau)</a> to LCA Delegates. If you need to contact
             someone about a problem or an issue with the VoIP servce, the best thing to do is find him at the conference, or tweet at him!</p>
          <p>If there is something that <b>critically needs to be investigated</b>, such has harassment, or unlawful activity, and you are unable to contact Rob,
	     please call <tt>333</tt>, or visit the registration desk and ask for assistance there - However, please try to avoid this unless it is CRITICALLY
	     urgent, as the organisers have enough on their plate, and aren't experts on VoIP!</p>
        </div>
      </div>
    </div>
  </div>
</div>


