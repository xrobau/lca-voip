<?php
/*
if (empty($_COOKIE['isinsecure'])) {
	include "warning.php";
	echo "</main>";
	return;
}
 */
?>

<div class='row'>
  <div class='col s10 offset-s1 m4 offset-m4'>
    <p class='center-align'><img src='/faktortel.svg'alt='LCA2019 VoIP was provided by OTW and FaktorTel'></p>
  </div>
  <div class='col s12'>
    <p class='center-align'>LCA2019 VoIP was provided by <a href='https://www.overthewire.com.au/?utm_source=linuxconf&utm_medium=mgmt'>Over the Wire</a> and <a href='https://faktortel.com.au/?utm_source=linuxconf&utm_medium=mgmt'>FaktorTel</a></p>
  </div>
</div>
 
<div class='row'>

  <div class='col s12 m6'>
    <div class='card'>
      <div class='card-content'>
        <span class='card-title'>Thanks, LCA! See you next year!</span>
	<p>We hope you enjoyed Linux.conf.au 2019. We'll hopefully be back, on the Gold Coast, for LCA2020!</p>
        <p>Thanks for having us, and, see you then!</p>
      </div>
    </div>
  </div>

  <div class='col s12 m6'>
    <div class='card'>
      <div class='card-content'>
        <span class='card-title'>LCA Plays, 2020?</span>
	<p>I'm looking for feedback for the 'LCAPlays' Twitch Stream for 2020.</p.
        <p> In 2019 we had over 100,000 keypresses for the 'LCA Plays Pokemon Crystal', and I think we can do better than that in 2020!</p>
        <p>Please make some suggestions to <a href='//twitter.com/xrobau'>Rob Thomas</a> and I'll update this page as we go.</p>
      </div>
    </div>
  </div>
</div>

