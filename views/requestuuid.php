<?php
$exten = $_REQUEST['exten']??'';
$pass = $_REQUEST['pass']??'';
$email = $_REQUEST['email']??'';

if (!$email || strlen($email) < 5 || strpos($email, '@') === false) {
	print "Invalid email '$email'. Sorry\n";
	return;
}

$spam = new xrobau\SpamCheck();

/* 

if (!$spam->checkspam($email)) {
	print "Maximum of one email per 5 mins per email address. Please try another\n";
	return;
}

 */

$lca = new \xrobau\LCA;
$lca->send_email($exten, $pass, $email);

?>

<div class='container'>
  <div class='row'>
    <div class='col s12'>
      <div class='card'>
        <div class='card-content'>
          <span class='card-title'>Check your email!</span>
          <p>If the credentials your entered were correct, you will have already received an email with the link.</p>
          <p>Note that many email services will (quite reasonably) mark a short email that is primarily a URL as spam, so please <b>check your spam folder</b>.</p>
          <p>If you believe you mis-typed something, you can re-sumbit, but note that there are ratelimits for sending emails - you can only send one email per 5 minutes to a distinct email address. You may need to use a different email address.</p>
          <p>When you receive the email, click on the link to access the management interface. That link is the key to your account, do not share it!</p>
        </div>
      </div>
    </div>
  </div>
</div>


