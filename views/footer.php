</main>

<footer class="page-footer green lighten-1">
  <div class='row'>
    <div class='col s10 offset-s1'>
      <p>The LCA2019 VoIP Server is provided as a best-effort service by <a href='//overthewire.com.au' target='_new'>Over the Wire</a> and
          <a href='//faktortel.com.au'>FaktorTel</a> and <b>is not supported or managed by LCA2019, or Linux Australia</b>. For any questions, please use the 'Contact' button on this page. Please ensure you have read <a href='/legal'>the Legal information</a>.</p>
    </div>
  </div>
</footer>
