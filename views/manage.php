<div class='row'>
  <div class='col s12'>
    <h4>Personalize your Extension</h4>
    <p>Before your can configure your CallerID Name, or Voicemail email, you need to authenticate yourself. To do so, enter the 5-digit extension 
	number and password on your VoIP card, and an email address. A link containing a UUID will be sent to that email address, which will
	allow you full control of that extension.</p>
    <p>Most passwords are 4-5 words, and no digits are used. Passwords <b>are case sensitive</b></p>
  </div>
</div>
<div class='row'>
  <form class='col s12' method='post' action='/requestuuid'>
    <div class='row'>
      <div class='input-field col s12 m4'>
        <input placeholder='00000' id='exten' name='exten' type="text">
        <label for='exten'>Your 5 digit Exten</label>
      </div>
      <div class='input-field col s12 m8'>
        <input placeholder='Password...' id='pass' name='pass' type="text">
        <label for='pass'>Your allocated password</label>
      </div>
      <div class='input-field col s12'>
        <input placeholder='email@example.com' id='email' name='email' type="text">
        <label for='email'>Email to receive management link</label>
      </div>
      <div class='col s12'>
        <div class='right'>
	  <button class=" btn waves-effect waves-light" type="submit" name="action" value="submit">Submit <i class="material-icons right">send</i> </button>
        </div>
      </div>
    </div>
  </form>
</div>



